package cz.martin.panacek.msd.warehouse.repository;

import cz.martin.panacek.msd.warehouse.model.PagedCollection;
import cz.martin.panacek.msd.warehouse.model.Product;

public interface ProductRepository extends AbstractRepository<Product> {

    PagedCollection<Product> findByCategory(long categoryId, int index, int size);
}
