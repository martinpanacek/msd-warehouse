package cz.martin.panacek.msd.warehouse.resource;

import cz.martin.panacek.msd.warehouse.exception.ConcurrentModificationException;
import cz.martin.panacek.msd.warehouse.exception.EntityNotFoundException;
import cz.martin.panacek.msd.warehouse.exception.IntegrityViolationException;
import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
@Provider
public class AppExceptionMapper implements ExceptionMapper<Throwable> {

    private final Logger LOG = LoggerFactory.getLogger(AppExceptionMapper.class);

    private static final Map<Class<? extends Throwable>, Function<Throwable, Response>> exceptionMappings =
            new HashMap<Class<? extends Throwable>, Function<Throwable, Response>>() {{

        put(EntityNotFoundException.class,
                exception -> {
                    EntityNotFoundException notFoundException = (EntityNotFoundException) exception;
                    return Response.status(Response.Status.NOT_FOUND).entity(
                            String.format("Cannot found entity '%s' with ID = '%d'.", notFoundException.getEntityType(), notFoundException.getId())).build();

                });

        put(IntegrityViolationException.class,
                exception -> Response.status(409).entity(exception.getMessage()).build());
        put(ConcurrentModificationException.class,
                        exception -> Response.status(409).entity(exception.getMessage()).build());

        put(IllegalArgumentException.class,
                exception -> Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build());
    }};

    @Override
    public Response toResponse(Throwable exception) {
        LOG.error("Unhandled exception.", exception);

        Function<Throwable, Response> exceptionMapping = this.exceptionMappings.get(exception.getClass());
        if (exceptionMapping != null) {
            return exceptionMapping.apply(exception);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected internal server error.").build();
    }
}
