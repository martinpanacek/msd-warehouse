package cz.martin.panacek.msd.warehouse.exception;

public class EntityNotFoundException extends PersistenceException {

    private Long id;
    private String entityType;

    public EntityNotFoundException(Throwable cause, Long id, String entityType) {
        super(cause);
        this.id = id;
        this.entityType = entityType;
    }

    public EntityNotFoundException(long id, String entityType) {
        this.id = id;
        this.entityType = entityType;
    }

    public Long getId() {
        return id;
    }

    public String getEntityType() {
        return entityType;
    }
}
