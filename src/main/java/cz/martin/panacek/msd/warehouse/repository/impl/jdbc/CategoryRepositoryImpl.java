package cz.martin.panacek.msd.warehouse.repository.impl.jdbc;

import cz.martin.panacek.msd.warehouse.exception.ConcurrentModificationException;
import cz.martin.panacek.msd.warehouse.exception.EntityNotFoundException;
import cz.martin.panacek.msd.warehouse.exception.IntegrityViolationException;
import cz.martin.panacek.msd.warehouse.model.Category;
import cz.martin.panacek.msd.warehouse.model.CategoryProductCollection;
import cz.martin.panacek.msd.warehouse.repository.CategoryRepository;
import cz.martin.panacek.msd.warehouse.repository.impl.jdbc.mapper.CategoryRowMapper;
import liquibase.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class CategoryRepositoryImpl extends AbstractRepositoryImpl<Category> implements CategoryRepository {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRepositoryImpl.class);

    private static final CategoryRowMapper CATEGORY_ROW_MAPPER = new CategoryRowMapper();
    public static final String SQL_CATEGORY_SUBTREE_RECURSION =
            "WITH RECURSIVE tree(id, parent_id, name) AS (\n" +
                    "SELECT id, parent_id, name \n" +
                    "FROM category\n" +
                    "WHERE id = ?\n" +
                    "UNION ALL\n" +
                    "SELECT c.id, c.parent_id, c.name\n" +
                    "FROM category  c\n" +
                    "JOIN tree ON tree.id = c.parent_id)\n";

    private static final String SQL_LIST_TREE =
                    SQL_CATEGORY_SUBTREE_RECURSION + "SELECT * FROM tree";
    private static final String SQL_DELETE_TREE =
            SQL_CATEGORY_SUBTREE_RECURSION + "DELETE FROM category WHERE id IN (SELECT id FROM tree)";


    @Autowired
    public CategoryRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }


    @Override
    protected String getTableName() {
        return "category";
    }

    @Override
    protected RowMapper getRowMapper() {
        return CATEGORY_ROW_MAPPER;
    }

    @Override
    protected Map<String, Object> toParamMap(Category modelObject) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", modelObject.getName());
        parameters.put("parent_id", modelObject.getParentId());
        return parameters;
    }

    @Override
    protected String getEntityType() {
        return "category";
    }

    @Override
    public Category create(Category entity) throws IntegrityViolationException {
        if (entity.getName() == null) {
            throw new IllegalArgumentException("Entity category must not have name == null.");
        }
        return super.create(entity);
    }

    @Override
    public Category find(final long id) throws EntityNotFoundException {
        Map<Long, Category> categoryById = getJdbcTemplate().query(String.format(SQL_LIST_TREE, getTableName()),
                new Object[]{id}, rs -> {
                    Map<Long, Category> categoryById1 = new HashMap<>();
                    while (rs.next()) {
                        Category current = new Category(
                                rs.getLong("id"),
                                rs.getString("name"),
                                (Long) rs.getObject("parent_id"),
                                null);
                        categoryById1.put(current.getId(), current);
                    }
                    return categoryById1;
                });

        Category result = null;
        for (Category current : categoryById.values()) {
            if (current.getId() == id) {
                result = current;
            }

            if (current.getParentId() != null) {
                Category parent = categoryById.get(current.getParentId());
                if (parent != null) {
                    parent.addSubcategory(current);
                }
            }
        }
        if (result == null) {
            throw new EntityNotFoundException(id, getEntityType());
        }
        return result;
    }

    protected String getDeleteSql() {
        return SQL_DELETE_TREE;
    }

    @Override
    public CategoryProductCollection getCategoryProducts(long categoryId) {
        List<Long> ids = getJdbcTemplate().queryForList("SELECT product_id FROM product_category WHERE category_id = ?",
                new Object[] {categoryId}, Long.class);
        return new CategoryProductCollection(ids, ids.hashCode());
    }

    @Override
    public void updateCategoryProducts(long categoryId, CategoryProductCollection collection) throws ConcurrentModificationException {
        // TODO check version before any modification

        // collection must be in same order to do not have conflict
        List<Long> productionIds = new ArrayList<>(collection.getProductIds());
        Collections.sort(productionIds);

        // remove items
        String sql = "DELETE FROM product_category WHERE category_id = ? ";
        if (productionIds.size() > 0) {
            sql = sql + String.format(" AND product_id NOT IN (%s)", StringUtils.repeat(",?", productionIds.size()).substring(1));
        }
        LinkedList<Long> params = new LinkedList(productionIds);
        params.addFirst(categoryId);
        getJdbcTemplate().update(sql, params.toArray());

        // add items
        sql = "INSERT INTO product_category (category_id, product_id) VALUES (?, ?) ON CONFLICT (category_id, product_id) DO NOTHING";
        getJdbcTemplate().batchUpdate(sql,
                productionIds.stream().map(item -> new Object[] {categoryId, item}).collect(Collectors.toList()));
    }


}
