package cz.martin.panacek.msd.warehouse.resource;

import cz.martin.panacek.msd.warehouse.model.Category;
import cz.martin.panacek.msd.warehouse.model.CategoryProductCollection;
import cz.martin.panacek.msd.warehouse.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Component
@Path("category")
public class CategoryResource extends AbstractResource {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryResource(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @POST
    public Response createCategory(Category category) {
        Category result = categoryRepository.create(category);
        return buildCreatedResponse(result);
    }

    @GET
    @Path("{categoryId}/product")
    public CategoryProductCollection getProductCategories(@PathParam("categoryId") long categoryId) {
        CategoryProductCollection collection = categoryRepository.getCategoryProducts(categoryId);
        return collection;
    }

    @PUT
    @Path("{categoryId}/product")
    public Response updateProductCategories(@PathParam("categoryId") long categoryId, CategoryProductCollection collection) {
        categoryRepository.updateCategoryProducts(categoryId, collection);
        return Response.noContent().build();
    }

    @GET
    @Path("{id}")
    public Category getCategory(@PathParam("id") long id) {
        return categoryRepository.find(id);
    }

    @DELETE
    @Path("{id}")
    public Response deleteCategory(@PathParam("id") long id) {
        categoryRepository.delete(id);
        return Response.noContent().build();
    }

}
