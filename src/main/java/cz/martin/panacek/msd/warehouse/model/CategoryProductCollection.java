package cz.martin.panacek.msd.warehouse.model;

import java.util.List;

public class CategoryProductCollection {

    private List<Long> productIds;
    private Integer version;

    private CategoryProductCollection() {
    }

    public CategoryProductCollection(List<Long> productIds, Integer version) {
        this.productIds = productIds;
        this.version = version;
    }

    public List<Long> getProductIds() {
        return productIds;
    }

    public CategoryProductCollection(Integer version) {
        this.version = version;
    }
}
