package cz.martin.panacek.msd.warehouse.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Category extends BaseEntity {

    private String name;
    private Long parentId;
    private List<Category> subcategories;

    public Category() {
    }

    @Override
    public Category cloneEntity() {
        return new Category(getId(), name, parentId,
                subcategories != null ? subcategories.stream().map(cat -> cat.cloneEntity()).collect(Collectors.toList()) : null);
    }

    public Category(Long id, String name, Long parentId) {
        super(id);
        this.name = name;
        this.parentId = parentId;
    }

    public Category(Long id, String name, Long parentId, List<Category> subcategory) {
        super(id);
        this.name = name;
        this.parentId = parentId;
        this.subcategories = subcategory;
    }

    public String getName() {
        return name;
    }

    public Long getParentId() {
        return parentId;
    }

    public List<Category> getSubcategories() {
        return subcategories;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setSubcategories(List<Category> subcategories) {
        this.subcategories = subcategories;
    }

    public void addSubcategory(Category subcategory) {
        if (this.subcategories == null) {
            this.subcategories = new ArrayList<>();
        }
        this.subcategories.add(subcategory);
    }

}
