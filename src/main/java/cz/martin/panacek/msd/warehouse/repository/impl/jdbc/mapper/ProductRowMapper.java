package cz.martin.panacek.msd.warehouse.repository.impl.jdbc.mapper;

import cz.martin.panacek.msd.warehouse.model.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductRowMapper implements RowMapper<Product> {
    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Product(rs.getLong("id"), rs.getString("name"), rs.getString("description"), (Long) rs.getObject("price"));
    }
}
