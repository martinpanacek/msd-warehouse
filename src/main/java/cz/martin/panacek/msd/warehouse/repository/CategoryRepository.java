package cz.martin.panacek.msd.warehouse.repository;

import cz.martin.panacek.msd.warehouse.model.Category;
import cz.martin.panacek.msd.warehouse.model.CategoryProductCollection;

public interface CategoryRepository extends AbstractRepository<Category> {

    CategoryProductCollection getCategoryProducts(long categoryId);

    void updateCategoryProducts(long categoryId, CategoryProductCollection collection);
}
