package cz.martin.panacek.msd.warehouse.repository.impl.jdbc;

import cz.martin.panacek.msd.warehouse.exception.EntityNotFoundException;
import cz.martin.panacek.msd.warehouse.exception.IntegrityViolationException;
import cz.martin.panacek.msd.warehouse.model.BaseEntity;
import cz.martin.panacek.msd.warehouse.repository.AbstractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.Map;

public abstract class AbstractRepositoryImpl<T extends BaseEntity> implements AbstractRepository<T> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AbstractRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    protected abstract String getTableName();

    protected abstract RowMapper<T> getRowMapper();

    protected abstract Map<String, Object> toParamMap(T modelObject);

    protected abstract String getEntityType();

    @Override
    public T create(T entity) throws IntegrityViolationException {
        if (entity.getId() != null) {
            throw new IllegalArgumentException("Entity to create cannot have explicitly specified ID.");
        }
        Map<String, Object> parameters = toParamMap(entity);

        Long id;
        try {
            id = (Long) new SimpleJdbcInsert(jdbcTemplate).withTableName(getTableName()).usingGeneratedKeyColumns("id")
                    .executeAndReturnKey(parameters);
        } catch (DataIntegrityViolationException e) {
            throw new IntegrityViolationException(e);
        }
        LOG.info("Entity {} with ID={} was created.", getEntityType(), id);

        T result = entity.cloneEntity();
        result.setId(id);


        return result;
    }

    @Override
    public T find(long id) throws EntityNotFoundException {
        try {
            return jdbcTemplate.queryForObject(String.format("SELECT * FROM %s WHERE id = ?", getTableName()),
                    new Object[]{id}, getRowMapper());
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(e, id, getEntityType());
        }
    }

    protected String getDeleteSql() {
        return String.format("DELETE FROM %s WHERE id = ?", getTableName());
    }

    @Override
    public void delete(long id) throws EntityNotFoundException {
        int count = jdbcTemplate.update(getDeleteSql(), id);
        if (count == 0) {
            throw new EntityNotFoundException(id, getEntityType());
        }
        LOG.info("Entity {} with ID={} was deleted.", getEntityType(), id);
    }

}
