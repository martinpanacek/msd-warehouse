package cz.martin.panacek.msd.warehouse.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CategoryBuilder {
    private Long id;
    private String name;
    private Long parentId;
    private List<Category> subcategories;

    public CategoryBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public CategoryBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CategoryBuilder setParentId(Long parentId) {
        this.parentId = parentId;
        return this;
    }

    public CategoryBuilder setSubcategories(List<Category> subcategories) {
        this.subcategories = subcategories;
        return this;
    }

    public CategoryBuilder addSubcategory(Category subcategory) {
        if (this.subcategories == null) {
            this.subcategories = new ArrayList<>();
        }
        this.subcategories.add(subcategory);
        return this;
    }

    public Category build() {
        return new Category(id,
                name,
                parentId,
                Collections.unmodifiableList(subcategories));
    }
}