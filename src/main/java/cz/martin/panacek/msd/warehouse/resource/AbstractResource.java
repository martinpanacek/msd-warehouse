package cz.martin.panacek.msd.warehouse.resource;

import cz.martin.panacek.msd.warehouse.model.BaseEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

public class AbstractResource {

    @Context
    private UriInfo uriInfo;

    protected <T extends BaseEntity> Response buildCreatedResponse(T entity) {
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Long.toString(entity.getId()));
        URI location = builder.build();

        Resource<T> representation = new Resource<T>(entity, new Link(location.toString(), Link.REL_SELF));
        return Response.created(location).entity(representation).build();
    }



}
