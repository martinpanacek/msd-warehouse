package cz.martin.panacek.msd.warehouse.exception;

public class IntegrityViolationException extends PersistenceException {

    public IntegrityViolationException() {
    }

    public IntegrityViolationException(Throwable cause) {
        super(cause);
    }

    public IntegrityViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
