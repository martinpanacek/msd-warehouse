package cz.martin.panacek.msd.warehouse.model;

import java.util.List;

public class PagedCollection<T extends BaseEntity> {

    private List<T> collection;
    private int index;
    private int size;

    public PagedCollection() {
    }

    public PagedCollection(List<T> collection, int index, int size) {
        this.collection = collection;
        this.index = index;
        this.size = size;
    }

    public List<T> getCollection() {
        return collection;
    }

    public int getIndex() {
        return index;
    }

    public int getSize() {
        return size;
    }
}
