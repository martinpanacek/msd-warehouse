package cz.martin.panacek.msd.warehouse.resource;

import cz.martin.panacek.msd.warehouse.model.PagedCollection;
import cz.martin.panacek.msd.warehouse.repository.ProductRepository;
import cz.martin.panacek.msd.warehouse.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.jaxrs.JaxRsLinkBuilder;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/product")
public class ProductResource extends AbstractResource {

    private final ProductRepository productRepository;

    @Autowired
    public ProductResource(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }



    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createProduct(Product product) {
        Product result = productRepository.create(product);
        return buildCreatedResponse(result);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Product getProduct(@PathParam("id") long id) {
        return productRepository.find(id);
    }

    @GET
    public PagedCollection<Product> getProductsForCategory(
            @QueryParam("category-id") long categoryId,
            @QueryParam("index") @DefaultValue("0") int index,
            @QueryParam("size") @DefaultValue("500") int size) {
        if (categoryId == 0) {
            throw new IllegalArgumentException("Category id must be specified");
        }
        return productRepository.findByCategory(categoryId, index, size);
    }

    @DELETE
    @Path("{id}")
    public Response deleteProduct(@PathParam("id") long id) {
        productRepository.delete(id);
        return Response.noContent().build();
    }

}
