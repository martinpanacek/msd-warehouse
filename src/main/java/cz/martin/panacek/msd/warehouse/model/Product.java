package cz.martin.panacek.msd.warehouse.model;

public class Product extends BaseEntity {

    private String name;
    private String description;
    private Long price;

    public Product() {
    }

    @Override
    public BaseEntity cloneEntity() {
        return new Product(getId(), name, description, price);
    }

    public Product(Long id, String name, String description, Long price) {
        super(id);
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Long getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
