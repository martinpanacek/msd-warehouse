package cz.martin.panacek.msd.warehouse.repository.impl.jdbc;


import cz.martin.panacek.msd.warehouse.model.PagedCollection;
import cz.martin.panacek.msd.warehouse.model.Product;
import cz.martin.panacek.msd.warehouse.repository.ProductRepository;
import cz.martin.panacek.msd.warehouse.repository.impl.jdbc.mapper.ProductRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductRepositoryImpl extends AbstractRepositoryImpl<Product> implements ProductRepository {

    public static final ProductRowMapper PRODUCT_ROW_MAPPER = new ProductRowMapper();

    public static final String SQL_PRODUCT_OF_CATEGORY =
            "WITH RECURSIVE categories(id, parent_id) AS (\n" +
            "SELECT id, parent_id\n" +
            "FROM category\n" +
            "WHERE id = ?\n" +
            "UNION ALL\n" +
            "SELECT c.id, c.parent_id\n" +
            "FROM category  c\n" +
            "JOIN categories ON categories.id = c.parent_id)\n" +
            "SELECT * FROM product p WHERE EXISTS (SELECT 1 FROM product_category pc WHERE p.id = pc.product_id AND category_id IN (SELECT id FROM categories)) ORDER BY id LIMIT ? OFFSET ?;";

    @Autowired
    public ProductRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTableName() {
        return "product";
    }

    @Override
    protected RowMapper<Product> getRowMapper() {
        return PRODUCT_ROW_MAPPER;
    }

    @Override
    protected Map<String, Object> toParamMap(Product modelObject) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", modelObject.getName());
        parameters.put("price", modelObject.getPrice());
        parameters.put("description", modelObject.getDescription());
        return parameters;
    }

    @Override
    protected String getEntityType() {
        return "product";
    }

    @Override
    public PagedCollection<Product> findByCategory(long categoryId, int index, int size) {
        List<Product> result = getJdbcTemplate().query(SQL_PRODUCT_OF_CATEGORY,
                new Object[] {categoryId, size, index}, PRODUCT_ROW_MAPPER);
        return new PagedCollection<>(result, index, result.size());
    }

}
