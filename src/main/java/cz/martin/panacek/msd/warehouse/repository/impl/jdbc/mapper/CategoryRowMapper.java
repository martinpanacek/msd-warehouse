package cz.martin.panacek.msd.warehouse.repository.impl.jdbc.mapper;

import cz.martin.panacek.msd.warehouse.model.Category;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryRowMapper implements RowMapper<Category> {

    @Override
    public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Category(rs.getLong("id"), rs.getString("name"), (Long) rs.getObject("parent_id"), null);
    }
}
