package cz.martin.panacek.msd.warehouse.repository;

import cz.martin.panacek.msd.warehouse.exception.EntityNotFoundException;
import cz.martin.panacek.msd.warehouse.exception.IntegrityViolationException;
import cz.martin.panacek.msd.warehouse.model.BaseEntity;

public interface AbstractRepository<T extends BaseEntity> {

    T create(T t) throws IntegrityViolationException;

    T find(long id) throws EntityNotFoundException;

    void delete(long id) throws EntityNotFoundException;
}
