package cz.martin.panacek.msd.warehouse.resource;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.stereotype.Component;

@Component
@EnableHypermediaSupport(type = {EnableHypermediaSupport.HypermediaType.HAL})
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(ProductResource.class);
        register(CategoryResource.class);

        register(AppExceptionMapper.class);
    }

}
