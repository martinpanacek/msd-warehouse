package cz.martin.panacek.msd.warehouse.test.support;

import cz.martin.panacek.msd.warehouse.model.Category;
import cz.martin.panacek.msd.warehouse.model.Product;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;


public class ProductMatcher extends TypeSafeDiagnosingMatcher<Product> {

    private Product expected;

    public ProductMatcher(Product expected) {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(Product actual, Description description) {
        if (actual == expected) {
            return true;
        }
        boolean match = true;

        if (expected.getId() != null ? !expected.getId().equals(actual.getId()) : actual.getId() != null) {
            appendDescription("id", expected.getId(), actual.getId(), description);
            match = false;
        }
        if (expected.getName() != null ? !expected.getName().equals(actual.getName()) : actual.getName() != null) {
            appendDescription("name", expected.getName(), actual.getName(), description);
            match = false;
        }
        if (expected.getDescription() != null ? !expected.getDescription().equals(actual.getDescription()) : actual.getDescription() != null) {
            appendDescription("description", expected.getDescription(), actual.getDescription(), description);
            match = false;
        }
        if (expected.getPrice() != null ? !expected.getPrice().equals(actual.getPrice()) : actual.getPrice() != null) {
            appendDescription("price", expected.getPrice(), actual.getPrice(), description);
            match = false;
        }
        return match;
    }

    private static void appendDescription(String fieldName, Object expected, Object actual, Description description) {
        description.appendText("\n")
                .appendText(fieldName).appendText("=")
                .appendValue(actual)
                .appendText(", expected value is ")
                .appendValue(expected)
                .appendText(";");
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Entity product fields matches");
    }
}
