package cz.martin.panacek.msd.warehouse.repository;

import cz.martin.panacek.msd.warehouse.exception.IntegrityViolationException;
import cz.martin.panacek.msd.warehouse.model.CategoryProductCollection;
import cz.martin.panacek.msd.warehouse.model.Product;
import cz.martin.panacek.msd.warehouse.test.support.CategoryMatcher;
import cz.martin.panacek.msd.warehouse.exception.EntityNotFoundException;
import cz.martin.panacek.msd.warehouse.model.Category;
import jersey.repackaged.com.google.common.collect.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@RunWith(SpringRunner.class)
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testCreate() {
        Category c1 = categoryRepository.create(new Category(null, "A", null));
        Category category = categoryRepository.find(c1.getId());
        Assert.assertThat(category, new CategoryMatcher(c1));

        Category c2 = categoryRepository.create(new Category(null, "B", c1.getId()));
        Category c3 = categoryRepository.create(new Category(null, "C", c2.getId()));

        category = categoryRepository.find(c1.getId());
        Assert.assertThat(category, new CategoryMatcher(c1));
        Assert.assertThat(category.getSubcategories().get(0), new CategoryMatcher(c2));
        Assert.assertThat(category.getSubcategories().get(0).getSubcategories().get(0), new CategoryMatcher(c3));

        // CORNER CASES

        // non-existing parent
        try {
            categoryRepository.create(new Category(null, "some name", 9999999999L));
        } catch (IntegrityViolationException e) {
            //expected
        }

        // create Category with given ID
        try {
            categoryRepository.create(new Category(12L, "some name", null));
        } catch (IllegalArgumentException e) {
            //expected
        }

        // create Category with num = null
        try {
            categoryRepository.create(new Category(null, null, null));
        } catch (IllegalArgumentException e) {
            //expected
        }

    }

    @Test
    public void testDelete() {
        Category c1 = categoryRepository.create(new Category(null, "First", null, null));
        Category c2 = categoryRepository.create(new Category(null, "Second", c1.getId(), null));
        categoryRepository.create(new Category(null, "Third", c2.getId(), null));

        categoryRepository.delete(c2.getId());
        Category category = categoryRepository.find(c1.getId());
        Assert.assertTrue(category.getSubcategories() == null || category.getSubcategories().isEmpty());


        // CORNER CASES

        // non-existing entity
        try {
            categoryRepository.delete(9999999L);
        } catch (EntityNotFoundException e) {
            // expected
        }

        // TODO other corner cases
    }

    @Test
    public void testUpdateCategoryProductsAndReadCategoryProducts() {
        Category c1 = categoryRepository.create(new Category(null, "A", null));
        Category c2 = categoryRepository.create(new Category(null, "B", c1.getId()));
        Category c3 = categoryRepository.create(new Category(null, "C", c2.getId()));

        Product p1 = productRepository.create(new Product(null, "A", "desc1", 100L));
        Product p2 = productRepository.create(new Product(null, "B", "desc2", 200L));
        Product p3 = productRepository.create(new Product(null, "C", "desc3", 300L));

        CategoryProductCollection collection = categoryRepository.getCategoryProducts(c1.getId());
        collection.getProductIds().add(p1.getId());
        collection.getProductIds().add(p2.getId());
        categoryRepository.updateCategoryProducts(c1.getId(), collection);


        CategoryProductCollection newCollection = categoryRepository.getCategoryProducts(c1.getId());
        Assert.assertThat(newCollection.getProductIds(), CoreMatchers.hasItems(collection.getProductIds().toArray(new Long[0])));

        newCollection.getProductIds().add(p3.getId());
        categoryRepository.updateCategoryProducts(c1.getId(), newCollection);
        collection = newCollection;
        newCollection = categoryRepository.getCategoryProducts(c1.getId());
        Assert.assertThat(newCollection.getProductIds(), CoreMatchers.hasItems(collection.getProductIds().toArray(new Long[0])));

        newCollection.getProductIds().remove(p1.getId());
        newCollection.getProductIds().remove(p2.getId());
        categoryRepository.updateCategoryProducts(c1.getId(), newCollection);
        collection = newCollection;
        newCollection = categoryRepository.getCategoryProducts(c1.getId());
        Assert.assertEquals(1, newCollection.getProductIds().size());
        Assert.assertThat(newCollection.getProductIds(), CoreMatchers.hasItems(collection.getProductIds().toArray(new Long[0])));
    }

}
