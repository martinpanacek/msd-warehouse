package cz.martin.panacek.msd.warehouse.resource;

import cz.martin.panacek.msd.warehouse.WarehouseApplication;
import cz.martin.panacek.msd.warehouse.model.*;
import cz.martin.panacek.msd.warehouse.test.support.CategoryMatcher;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WarehouseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WarehouseApplicationRestTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testAssignProductToCategoryAndReadProductsOfCategory() {
        Category c1 = createCategory(new Category(null, "A" + UUID.randomUUID().toString(), null));
        Category c11 = createCategory(new Category(null, "B" + UUID.randomUUID().toString(), c1.getId()));
        Category c111 = createCategory(new Category(null, "B" + UUID.randomUUID().toString(), c11.getId()));
        Category c2 = createCategory(new Category(null, "C" + UUID.randomUUID().toString(), null));

        Category category = readCategory(c1.getId());
        Assert.assertThat(category, new CategoryMatcher(c1));
        Assert.assertThat(category.getSubcategories().get(0), new CategoryMatcher(c11));
        Assert.assertThat(category.getSubcategories().get(0).getSubcategories().get(0), new CategoryMatcher(c111));

        Product p1 = createProduct(new Product(null, "A" + UUID.randomUUID().toString(), "desc1", 100L));
        Product p2 = createProduct(new Product(null, "B" + UUID.randomUUID().toString(), "desc2", 200L));
        Product p3 = createProduct(new Product(null, "C" + UUID.randomUUID().toString(), "desc3", 300L));


        ResponseEntity<CategoryProductCollection> responseCollection = testRestTemplate.getForEntity("/category/" + c1.getId() + "/product", CategoryProductCollection.class);
        CategoryProductCollection collection = responseCollection.getBody();
        collection.getProductIds().add(c1.getId());
        HttpEntity<CategoryProductCollection> requestCollection = new HttpEntity<>(collection);
        testRestTemplate.put("/category/" + c1.getId() + "/product", requestCollection, CategoryProductCollection.class);

        Map<String, Object> params = new HashMap<>();
        params.put("category-id", c1.getId());
        ResponseEntity pageResponse = testRestTemplate.getForEntity("/product", String.class, params);
        pageResponse.getBody();
    }

    private Category readCategory(long id) {
        ResponseEntity<Category> response = testRestTemplate.getForEntity("/category/" + id, Category.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        return response.getBody();
    }

    private Category createCategory(Category newCategory) {
        return createEntity("/category", newCategory, Category.class);
    }

    private Product createProduct(Product newProduct) {
        return createEntity("/category", newProduct, Product.class);
    }

    private <T extends BaseEntity> T createEntity(String url, T entity, Class<T> entityClass) {
        HttpEntity<T> categoryRequest = new HttpEntity<>(entity);
        ResponseEntity<T> response = testRestTemplate.postForEntity(url, categoryRequest, entityClass);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        return response.getBody();
    }
}
