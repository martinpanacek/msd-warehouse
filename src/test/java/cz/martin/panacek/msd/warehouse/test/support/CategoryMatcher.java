package cz.martin.panacek.msd.warehouse.test.support;

import cz.martin.panacek.msd.warehouse.model.Category;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * Matcher for category entity which does not match subcategories.
 */
public class CategoryMatcher extends TypeSafeDiagnosingMatcher<Category> {

    private Category expected;

    public CategoryMatcher(Category expected) {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(Category actual, Description description) {
        if (actual == expected) {
            return true;
        }
        boolean match = true;

        if (expected.getParentId() != null ? !expected.getParentId().equals(actual.getParentId()) : actual.getParentId() != null) {
            appendDescription("parentId", expected.getParentId(), actual.getParentId(), description);
            match = false;
        }
        if (expected.getId() != null ? !expected.getId().equals(actual.getId()) : actual.getId() != null) {
            appendDescription("id", expected.getId(), actual.getId(), description);
            match = false;
        }
        if (expected.getName() != null ? !expected.getName().equals(actual.getName()) : actual.getName() != null) {
            appendDescription("name", expected.getName(), actual.getName(), description);
            match = false;
        }
        return match;
    }

    private static void appendDescription(String fieldName, Object expected, Object actual, Description description) {
        description.appendText("\n")
                .appendText(fieldName).appendText("=")
                .appendValue(actual)
                .appendText(", expected value is ")
                .appendValue(expected)
                .appendText(";");
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Entity category fields (except subcategories) matches");
    }
}
