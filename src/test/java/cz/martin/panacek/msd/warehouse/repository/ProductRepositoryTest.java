package cz.martin.panacek.msd.warehouse.repository;

import cz.martin.panacek.msd.warehouse.exception.EntityNotFoundException;
import cz.martin.panacek.msd.warehouse.model.Category;
import cz.martin.panacek.msd.warehouse.model.CategoryProductCollection;
import cz.martin.panacek.msd.warehouse.model.PagedCollection;
import cz.martin.panacek.msd.warehouse.model.Product;
import cz.martin.panacek.msd.warehouse.test.support.ProductMatcher;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Transactional
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@RunWith(SpringRunner.class)
public class ProductRepositoryTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testCreate() {
        Product p1 = productRepository.create(new Product(null, "A", "desc1", 100L));
        Assert.assertThat(p1, new ProductMatcher(productRepository.find(p1.getId())));
        Product p2 = productRepository.create(new Product(null, "B", "desc2", 200L));
        Assert.assertThat(p1, new ProductMatcher(productRepository.find(p1.getId())));
        Assert.assertThat(p2, new ProductMatcher(productRepository.find(p2.getId())));

        // TODO CORNER CASES
    }

    @Test
    public void testDelete() {
        Product p1 = productRepository.create(new Product(null, "A", "desc1", 100L));
        Assert.assertThat(p1, new ProductMatcher(productRepository.find(p1.getId())));

        productRepository.delete(p1.getId());
        try {
            productRepository.find(p1.getId());
        } catch (EntityNotFoundException e) {
            //expected
        }

        // TODO CORNER CASES
    }

    @Test
    public void findByCategory() {
        Category c1 = categoryRepository.create(new Category(null, "A", null, null));
        Category c11 = categoryRepository.create(new Category(null, "B", c1.getId(), null));
        Category c2 = categoryRepository.create(new Category(null, "C", null, null));
        Category c3 = categoryRepository.create(new Category(null, "D", null, null));

        Product p1 = productRepository.create(new Product(null, "A", "desc1", 100L));
        Product p2 = productRepository.create(new Product(null, "B", "desc2", 200L));
        Product p3 = productRepository.create(new Product(null, "C", "desc3", 300L));

        CategoryProductCollection collection = categoryRepository.getCategoryProducts(c1.getId());
        collection.getProductIds().add(p1.getId());
        categoryRepository.updateCategoryProducts(c1.getId(), collection);
        collection = categoryRepository.getCategoryProducts(c11.getId());
        collection.getProductIds().add(p2.getId());
        categoryRepository.updateCategoryProducts(c11.getId(), collection);
        collection = categoryRepository.getCategoryProducts(c2.getId());
        collection.getProductIds().add(p3.getId());
        categoryRepository.updateCategoryProducts(c2.getId(), collection);
//        categoryRepository.assignProductToCategory(c1.getId(), p1.getId());
//        categoryRepository.assignProductToCategory(c11.getId(), p2.getId());
//        categoryRepository.assignProductToCategory(c2.getId(), p3.getId());

        PagedCollection<Product> page = productRepository.findByCategory(c1.getId(), 0, 100);
        Assert.assertEquals(0, page.getIndex());
        Assert.assertEquals(2, page.getSize());
        Assert.assertThat(p1, new ProductMatcher(page.getCollection().get(0)));
        Assert.assertThat(p2, new ProductMatcher(page.getCollection().get(1)));

        page = productRepository.findByCategory(c11.getId(), 0, 100);
        Assert.assertEquals(0, page.getIndex());
        Assert.assertEquals(1, page.getSize());
        Assert.assertThat(p2, new ProductMatcher(page.getCollection().get(0)));

        page = productRepository.findByCategory(c3.getId(), 0, 100);
        Assert.assertEquals(0, page.getIndex());
        Assert.assertEquals(0, page.getSize());



        // TODO CORNER CASES
    }
}
