Warehouse Application
=======================

Requirements
------------
#### General guidelines
Create application in Java
Use frameworks and libraries of your choice.
The solution should provide approach to testing (sample tests) but extensive testing of the whole application is not required.
Please push the solution to GitHub or BitBucket.

####Warehouse
Create a simple application Warehouse.
Application works with following entities:
* Category with following attributes:
    * Name (is unique)
    * Parent category identification (categories form a tree)
    * Product with following attributes:
        * Name
        * Description
        * Price
The relation between Categories and Products is many-to-many. One product can belong to multiple categories. One category can contain multiple products.
Design the database schema and create an appropriate persistence layer in the application. Propose and implement a RESTful service that supports following functions:
Add a new category
Add a new product
Assign/remove product from category
Delete a category
List category tree
List products in a selected category

Design and Implementation Notes
-------------------------------
- as part of task is database schema design I implemented it with relation database although IMH it would be better to implement it with NoSql database
- hierarchical queries can be slow for deep trees
    - mitigation: 
       - add caching 
       - if depth of tree would be limited to small number there is possible to use materialized path
       - other option is to use "nested set model" for faster queries (but creation of new category would be more complex) 


DB Schema design
----------------

* Category
    * id (long) PK
    * parentId (long) FK
    * name (varchar)


* Product
    * id (long) PK
    * name (varchar)
    * description (varchar)
    * price (long)


* ProductCategory
    * product_id (long) FK PK
    * category_id (long) FK PK

RESTful API Design
-----------------

| Use Case  | Http Method | URI | Request body
|---|---|---|---|
| Add new category                      | POST  | /category                     | category JSON without subcategories 
| Remove category                       | DELETE| /category/{id}                | no content
| Add new product                       | POST  | /product                      | product JSON
| Add new product                       | DELETE| /product/{id}                 | no content
| Assign/remove product to/from category| PUT   | /category/{id}/product        | collection of product IDs 
| List category tree                    | GET   | /category/{id}                | no content
| List products in selected category    | GET   | /product?category-id={id}     | no content

How to start Warehouse service
------------------------------
- configure database connecting in /resources/application.properties
- execute WarehouseApplication.main()

Improvements
-------------
- pagination for all resource where needed
- error should have JSON representation
- REST with HATEOAS
- Swagger for documentation
- caching
- REST test should clean up all data
- modularization of production code and test code
- for PUT operation it is necessary to implement optimistic lock (using version)
- validation in resources





